# Enable ssl auth on server
sudo a2enmod ssl
sudo mkdir /etc/apache2/ssl.crt/
cd /etc/apache2/ssl.crt/

echo '-----BEGIN RSA PRIVATE KEY-----
MIIEowIBAAKCAQEAtQ3Oe/V8xKI105VDVtdZ/+vgaQhzu7Rjy+u4cacIUZeIYOSH
WYQWw1aMVabRb4wG82tdTBBkBjd19ql4uJSywVP+1dGGY2/2fBKIioeXXyHtj3yH
whLwl7dN2UpFVsS7HSJhGBu59XDvEstXSz4G5Z30SCQK3ylYq6fEvU0apsqYec4H
X2n+POAKkcK8O50nIopQ8qKfj2UZzyjf4Ajay3cXAymSvt7cGhUi7xqq7VxDOw0k
IypxBCCL2BK+omdAiCW6vH8DGclPYODPUA4TeQJIMi1hzMIEQTuEba1Mhxz+utkC
76keoSjqwoSlQjPXMUzL9PmhYq/iabkvU700gQIDAQABAoIBACSQ6Ub3yX8dE6nr
13HOhIq/ZM9Rua4LPKyOqu2pQnDYHd8TyJI3rzE8ZZNW4a2Zpj27TVhzZwAqa1Sp
3YvJ3GBXSnx8mJu0nR9pztHLmLLJ82SrTe/QS28YwkxER8TCbtfvfZSU5PXBHUe6
caLsoA/32sJHJTr06segVoe07tEIeq/FrZbvFD7k2D9cVMx+ean2Bt+lBAWNr6wQ
TITRq4W+FM50bXL2v7/9rolTyHPAb1tFLhW+gwlm5aPPbSRSXsMcI+jZni1vA5pV
EHlMmhgdCU7zbKlICVPKQHWDDIcYk2uO4k5mPKuC++w+qXkb94gDOaA5Od5l1DFh
xxEf0wECgYEA5AuYMyq7qaJR4Ft1dYst5UXpv6UlZNq19CgL36lLwBAx4CABwq6b
ALi5aXsXyvIXW9l+ZcCHEfXcovCysMOrGMTIVQDV1/R3KSWDqhT2XGjycSzZNCBG
43ap0RrMVXemV6MJtSrM6sXXGN5RGebNFrPEslRQawmQcxCKcvzNuKkCgYEAyz+N
YScNRd3Do/rzr0U4mJRMEbIkk2S7u9x+2yJtQeOzfLzCPBOWgeTF+YfFkU+Akb52
WPsc0aJ1cRc+TgKkRG/VBbRIGDWy/W9DQFcT75pAjV6rw0iW7c+u8IYaj/gT9sav
243EFS+7M542rt8z/P1wjK39bAvrKePcBiq0TBkCgYEA1URqoF90hZ5CCBpAbR8u
Wt7ZTds6b2mDBMV25ULEjugWXM5SFymH2FLpQ1ZU1/00DzgF108Igql2IAQOePVk
y5Ms7yIK2x3LWvW0fpmAFMgpA+K3duJAqXVONzpiogN10zusVnuijGVdVeqD2j9Y
ycRxTX1Af/m/13w+nm8CTZECgYBf9narO0QCC83We6NYyscJC2PLKyAn03TsIxRm
JKAPbxIMwBa5RMpYNArhYSCAFlhu981q/81AkYL/zWimboreoSqkEdXEUH5dOI45
L6QDCgKWfO9+OaTBTcCcYcTi2s+aQX9Am3PMSQgLPBU3hb1HIuZkFbZEsG/tDF+T
LdCI6QKBgG31Uu5jIGhwGPWuys2pkhpxFcEs896Qvu/9hnCQJHU9C8zGXO6kJS2Q
bOpPUAJKFq7Zk+rJDtBblvRFo+GKtWU2PYItT0mGgaC7n6MZvsNFNlNoxYRKWF/n
XmagHdYQaXZxNTrrYTdZub9HRpiFYOfNXalVydRRGUKjPDhuiubO
-----END RSA PRIVATE KEY-----' | sudo tee /etc/apache2/ssl.crt/rootCA.key

sudo openssl req -x509 -new -nodes -key /etc/apache2/ssl.crt/rootCA.key -days 7300 -out /etc/apache2/ssl.crt/rootCA.pem -subj "/C=GB/ST=London/L=London/O=/OU=/CN=*.scrimservers.io"
sudo cp /etc/apache2/ssl.crt/rootCA.pem /etc/apache2/ssl.crt/ca-bundle.crt

sudo mkdir -p /var/www/auth/ca/
echo '[ ca ]
default_ca      = CA_default
[ CA_default ]
dir            = /var/www/auth/ca
database       = $dir/index.txt
new_certs_dir  = $dir/newcerts
certificate    = /etc/apache2/ssl.crt/rootCA.pem
serial         = $dir/serial
private_key    = /etc/apache2/ssl.crt/rootCA.key
RANDFILE       = $dir/private/.rand
default_days   = 3650
default_crl_days= 60
default_md     = sha1
policy         = policy_any
email_in_dn    = yes
name_opt       = ca_default
cert_opt       = ca_default
copy_extensions = none
[ policy_any ]
countryName            = supplied
stateOrProvinceName    = optional
organizationName       = optional
organizationalUnitName = optional
commonName             = supplied
emailAddress           = optional' | sudo tee /var/www/auth/ca/ca.conf


#cd /var/www
#echo '

#SSLVerifyClient require' | sudo tee -a .htaccess
#echo 'SSLVerifyDepth 1' | sudo tee -a .htaccess
#echo 'SSLOptions +ExportCertData +StdEnvVars' | sudo tee -a .htaccess

cd /var/www/auth/ca/
sudo touch index.txt
sudo mkdir newcerts
echo '1000' | sudo tee serial
echo 'Deny from all' | sudo tee -a .htaccess
sudo chown -R www-data .


#sudo sed -i.bak 's/#SSLCACertificateFile/SSLCACertificateFile/' /etc/apache2/sites-available/default-ssl.conf
#sudo sed -i.bak 's/AllowOverride None/AllowOverride All/g' /etc/apache2/apache2.conf


sudo service apache2 restart