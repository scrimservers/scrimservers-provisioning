#!/bin/bash
sudo cp -R ~/.ssh/* /var/www/.ssh

sudo apt-get update ; sudo apt-get install apache2 libapache2-mod-php5 curl git php5-cli php5-curl php5-mcrypt php5-mysql libpcre3-dev php5-dev php-pear -y

sudo sed -i 's;www-data:x:33:33:www-data:/var/www:/usr/sbin/nologin;www-data:x:33:33:www-data:/var/www:/bin/bash;' /etc/passwd
echo 'www-data:d9'| sudo chpasswd

sudo sed -i 's|[#]*PasswordAuthentication no|PasswordAuthentication yes|g' /etc/ssh/sshd_config
sudo sed -i 's|UsePAM no|UsePAM yes|g' /etc/ssh/sshd_config

sudo service apache2 stop

# Pull git repo
cd /tmp
sudo rm -rf /tmp/scrimservers

sudo echo -e "Host bitbucket.org\n\tStrictHostKeyChecking no\n" >> ~/.ssh/config
sudo git clone ssh://git@bitbucket.org/scrimservers/scrimservers.git
sudo cp -R /tmp/scrimservers/* /var/www
sudo cp /tmp/scrimservers/.htaccess /var/www

sudo sed -i 's|DocumentRoot /var/www/html|DocumentRoot /var/www|g' /etc/apache2/sites-available/000-default.conf
sudo sed -i '/<Directory \/var\/www\/>/,/<\/Directory>/ s/AllowOverride None/AllowOverride all/' /etc/apache2/apache2.conf

sudo pecl install SPL_Types
echo extension=spl_types.so | sudo tee /etc/php5/mods-available/spl_types.ini

sudo php5enmod spl_types
sudo php5enmod curl
sudo php5enmod mcrypt
sudo php5enmod mysql

sudo a2enmod rewrite

cd /var/www
curl -sS https://getcomposer.org/installer | sudo php -- --install-dir=/usr/local/bin --filename=composer

sudo php /usr/local/bin/composer install
sudo php /usr/local/bin/composer update

sudo chown -R www-data:www-data /var/www

sudo service apache2 restart
sudo service ssh restart