# Lighttpd based maint deamon

sudo apt-get update && sudo apt-get install nginx lighttpd -y

echo '
# /usr/share/doc/lighttpd/proxy.txt
server.modules   += ( "mod_proxy" )
$SERVER["socket"] == ":80" {
    server.port = 80
    proxy.balance = "fair"
    proxy.server  = ( "" => ( ( "host" => "127.0.0.1", "port"=> 679 ),( "host" => "127.0.0.1", "port"=> 1738 ) ) )
}' | sudo tee /etc/lighttpd/conf-available/10-proxy.conf
sudo lighttpd-enable-mod proxy

echo "

server {
    listen 1738;
    listen [::]:1738;

    root /var/www/_inc/maint;
    index index.html index.htm;

    location / {
        try_files $uri $uri/ =404;
    }
}" | sudo tee /etc/nginx/sites-available/default

sudo sed -i 's|*:80|*:679|g' /etc/apache2/sites-enabled/000-default.conf
sudo sed -i 's|80|679|g' /etc/apache2/ports.conf

sudo service apache2 restart
sudo service lighttpd restart
sudo service nginx restart