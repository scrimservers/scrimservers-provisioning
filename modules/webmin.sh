#!/bin/bash

echo "deb http://download.webmin.com/download/repository sarge contrib" | sudo tee -a /etc/apt/sources.list
echo "deb http://webmin.mirror.somersettechsolutions.co.uk/repository sarge contrib" | sudo tee -a /etc/apt/sources.list

wget -q http://www.webmin.com/jcameron-key.asc -O- | sudo apt-key add -
sudo apt-get update && sudo apt-get install webmin -y


sudo sed -i 's|theme=blue-theme|theme=authentic-theme|g' /etc/webmin/config
sudo sed -i 's|theme=blue-theme|theme=authentic-theme|g' /etc/webmin/miniserv.conf

sudo service webmin restart