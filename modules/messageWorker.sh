#!/bin/bash

sudo apt-get update ; sudo apt-get install php5-cli -y

sudo apt-get install python-setuptools -y
sudo apt-get install python-pip -y

sudo pip install python-neutronclient
sudo pip install requests==2.5.3