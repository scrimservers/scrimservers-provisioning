echo '<IfModule mod_ssl.c>
        <VirtualHost *:443>
                ServerAdmin webmaster@localhost
                DocumentRoot /var/www

                ErrorLog ${APACHE_LOG_DIR}/error.log
                CustomLog ${APACHE_LOG_DIR}/access.log combined
                SSLEngine on

                SSLCertificateFile    /etc/apache2/ssl.crt/rootCA.pem
                SSLCertificateKeyFile /etc/apache2/ssl.crt/rootCA.key
                SSLCACertificateFile  /etc/apache2/ssl.crt/ca-bundle.crt

                SSLVerifyClient require
                SSLVerifyDepth 1
                SSLOptions +ExportCertData +StdEnvVars
                
                SetEnv APPLICATION_ENV "development"
        </VirtualHost>
</IfModule>' | sudo tee /etc/apache2/sites-available/default-ssl.conf

sudo a2enmod ssl
sudo a2ensite default-ssl.conf
sudo service apache2 restart