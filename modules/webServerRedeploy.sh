cd /tmp
sudo rm -rf /tmp/scrimservers
sudo echo -e "Host bitbucket.org\n\tStrictHostKeyChecking no\n" >> ~/.ssh/config
sudo git clone ssh://git@bitbucket.org/scrimservers/scrimservers.git
sudo rm -R /var/www
sudo mkdir /var/www
sudo cp -R /tmp/scrimservers/* /var/www
sudo cp /tmp/scrimservers/.htaccess /var/www
cd /var/www
curl -sS https://getcomposer.org/installer | sudo php -- --install-dir=/usr/local/bin --filename=composer
sudo php /usr/local/bin/composer install
sudo php /usr/local/bin/composer update
sudo chown -R www-data:www-data /var/www
sudo service apache2 restart