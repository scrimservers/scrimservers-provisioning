GITSERVER="10.240.0.2"
echo "`date -u` Begin"

## Set up front end server
curl "http://$GITSERVER/scrimservers/scrimservers/scrimserversprovisioning/raw/master/modules/webServer.sh" | sudo bash

## Set up clientAuth generator
curl "http://$GITSERVER/scrimservers/scrimservers/scrimserversprovisioning/raw/master/modules/clientAuth.sh" | sudo bash

## Set up redeployment script
curl "http://$GITSERVER/scrimservers/scrimservers/scrimserversprovisioning/raw/master/modules/webServerRedeploy.sh" | sudo tee /home/dominic/redeploy
curl "http://$GITSERVER/scrimservers/scrimservers/scrimserversprovisioning/raw/master/modules/clientAuth.sh" | sudo tee -a /home/dominic/redeploy
sudo chmod +x /home/dominic/redeploy

echo "`date -u` Fin"